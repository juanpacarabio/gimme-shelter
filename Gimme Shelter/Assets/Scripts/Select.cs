using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Select : MonoBehaviour
{
    public bool selected;
    public int ID; // 0 = Vac�o, 1 = Madera, 2 = Concreto, 3 = Metal
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SelectThis()
    {
        GameObject[] seleccionables = GameObject.FindGameObjectsWithTag("Select");
        foreach(GameObject select in seleccionables)
        {
            if (select != this)
            {
                select.GetComponent<Select>().selected = false;
                select.GetComponent<Outline>().OutlineColor = Color.black;
            }
            ControlJugador.instance.bloqueSelecto = ID;
        }
        selected = true;
        this.GetComponent<Outline>().OutlineColor = Color.green;
    }

    private void OnMouseEnter()
    {
        if (selected == false)
        {
            this.GetComponent<Outline>().OutlineColor = Color.white;

        }
    }
    private void OnMouseExit()
    {
        if (selected == false)
        {
            this.GetComponent<Outline>().OutlineColor = Color.black;
        }
    }
}
