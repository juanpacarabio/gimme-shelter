using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    public GameObject[] bloques;
    public int bloqueSelecto;
    GameObject objetoTocado;
    public static ControlJugador instance;
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }

    void Start()
    {
        bloqueSelecto = 0;
    }


    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100))
            {
                Debug.DrawLine(ray.origin, hit.point);
                objetoTocado = hit.transform.gameObject;
                if (objetoTocado.CompareTag("Slot"))
                {
                    Remplazar(objetoTocado);
                }else if (objetoTocado.CompareTag("Select") && objetoTocado.GetComponent<Select>().selected == false)
                {
                    objetoTocado.GetComponent<Select>().SelectThis();
                }
            }
        }
        if (Input.GetMouseButtonDown(1))
        {
            GameObject[] seleccionables = GameObject.FindGameObjectsWithTag("Select");
            foreach(GameObject seleccionable in seleccionables)
            {
                seleccionable.GetComponent<Select>().selected = false;
                seleccionable.GetComponent<Outline>().OutlineColor = Color.black;
            }
            bloqueSelecto = 0;
        }
    }
    void Remplazar(GameObject remplazado)
    {
        remplazado.GetComponent<Collider>().enabled = false;
        Vector3 posicion = remplazado.transform.position;
        Quaternion rotacion = remplazado.transform.rotation;
        Instantiate(bloques[bloqueSelecto], posicion, rotacion);
        Debug.Log("Se remplaz� " + remplazado);
        Destroy(remplazado);
    }
}
