using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TooltipManager : MonoBehaviour
{
    public static TooltipManager instance;
    public TextMeshProUGUI texto;
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }
    void Start()
    {
        OcultarTooltip();
    }


    void Update()
    {
        transform.position = Input.mousePosition;
    }

    public void MostrarTooltip(string mensaje)
    {
        gameObject.SetActive(true);
        texto.text = mensaje;
    }

    public void OcultarTooltip()
    {
        gameObject.SetActive(false);
        texto.text = string.Empty;
    }

}
