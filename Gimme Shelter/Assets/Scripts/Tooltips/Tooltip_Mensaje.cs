using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tooltip_Mensaje : MonoBehaviour
{
    public string texto;
    string textoSalida;
    private void Update()
    {
        if (this.GetComponent<Select>().selected == true)
        {
            textoSalida = "Right Click to Cancel";
        }else
        {
            textoSalida = texto;
        }
    }
    private void OnMouseEnter()
    {
        TooltipManager.instance.MostrarTooltip(textoSalida);
    }

    private void OnMouseExit()
    {
        TooltipManager.instance.OcultarTooltip();
    }
}
