using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blocks : MonoBehaviour
{
    public int health;
    public int cost;
    public bool up, down, right, left;
    public GameObject block;
   
    private void OnMouseEnter()
    {
        block.GetComponent<Outline>().enabled = true;
    }

    private void OnMouseExit()
    {
        block.GetComponent<Outline>().enabled = false;
    }

    //private void Update()
    //{
    //    if(up == false && down == false && left == false && right == false)
    //    {
    //        block.GetComponent<Rigidbody>().useGravity = true;
    //        block.GetComponent<Rigidbody>().isKinematic = false;
    //    }
    //    else
    //    {
    //        block.GetComponent<Rigidbody>().useGravity = false;
    //        block.GetComponent<Rigidbody>().isKinematic = true;
    //    }
    //}
}
