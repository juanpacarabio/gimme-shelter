using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Right : MonoBehaviour
{

    public GameObject block;
    bool activado;
    void Update()
    {
        activado = (Physics.Raycast(this.transform.position, Vector3.right, 0.5f));
        Debug.Log(activado);
        block.GetComponent<Blocks>().right = activado;
    }
}
