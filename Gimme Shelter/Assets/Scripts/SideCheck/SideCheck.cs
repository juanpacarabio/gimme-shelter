using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideCheck : MonoBehaviour
{
    public GameObject block;
    public bool activado;
    private void OnTriggerEnter(Collider other)
    {
        if (other.name.Substring(0,5) == "Block")
        {
            activado = true;
        }
        else if (other == null)
        {
            activado = false;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.name.Substring(0, 5) == "Block")
        {
            activado = false;
        }
    }
}
